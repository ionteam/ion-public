
# 24 July 2015

## iolfeed-publisher

`iolfeed-publisher` should now have same consistent JSON format as `iolcategory-publisher`

http://beta.ion1.iol.io/iolf/f/0/5/sport

http://beta.ion1.iol.io/iolc/c/0/5/sport

## iolrefeeder

the feed ingester aka `iolrefeeder` has slightly different format with props `[pubDate, title, items]` - and it's item are iolmobile/RSS style naming `[id, title, description]` whereas `iolfeed-publisher` and `iolcategory-publisher` translate that to Baobab conventions `[contentKey, abstract, headline]` for our `ion` (ReactJS) frontend

http://beta.ion1.iol.io/feed/f/tonight-tvradio-localnews

http://beta.ion1.iol.io/iolf/f/0/5/tonight-tvradio-localnews

http://beta.ion1.iol.io/iolc/c/0/5/tonight/tvradio/localnews

### iolrefeeder RSS legacy

`iolrefeeder` is one of oldest services, originally for IOL feeds, but now also the Polopoly ingester for IOLbeta.

As such it's a bit too overloaded with functionality that could and should be separate services, to improve modularity.

The intention of  `iolrefeeder`  publishing JSON data is really for debugging - it's purpose was to publish RSS feeds, which it does.

Its code has been reorganised to prepare to hopefully splitting its RSS publishing out into separate customisable RSS publishing services for partners e.g. iolsportzone and my.independent app

### iolrefeeder on-demand article publisher

Note that `iolrefeeder` is best source for getting article json, because it uses `iolscraper` and so can scrape article on-demand if not previously imported - e.g. service a hit from SERP, or even related story

We should split the article publishing out of `iolrefeeder,` so that all that is left is the ingester.

### Immutable vs refactoring

An alternative approach to take would be to "clone" `iolrefeeder` into `polopoly-article-publisher` and `polopoly-ingester` and refactor those down to a their specific functionality.

We would then consider `refeeder` an immutable legacy service which we can retire sometime.

That is taking a somewhat "immutable" approach to evolution.

### Versioning for evolving immutable services

We should introduce versioning to all these services, e.g. `/iolf1/` meaning `iolfeed-publisher` version 1.

When we are going to produce version 2, we tag the repo with a version e.g. `v1` and then the `HEAD` is `v2` which we can serve as `/iolf2/`
